package students;

import org.hibernate.Session;
import students.entity.Course;
import students.entity.Details;
import students.entity.Student;
import students.util.HibernateUtil;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

//        // creating and saving students
//        Student student1 = new Student();
//        student1.setName("Vitalij");
//        student1.setEmail("vtl");
//        student1.setAge(35);
//        Student student2 = new Student();
//        student2.setName("Tadas");
//        student2.setEmail("tds");
//        student2.setAge(27);
//        Student student3 = new Student();
//        student3.setName("Andrius");
//        student3.setEmail("and");
//        student3.setAge(20);
//
//        session.save(student1);
//        session.save(student2);
//        session.save(student3);
//
//        // creating and saving courses
//        Course course1 = new Course();
//        course1.setName("Java");
//        Course course2 = new Course();
//        course2.setName("Python");
//        Course course3 = new Course();
//        course3.setName("C#");
//
//        session.save(course1);
//        session.save(course2);
//        session.save(course3);
//
//        // creating student sets
//        Set<Student> studentSet1 = new HashSet<>();
//        studentSet1.add(student1);
//        studentSet1.add(student2);
//        Set<Student> studentSet2 = new HashSet<>();
//        studentSet2.add(student1);
//        studentSet2.add(student3);
//        Set<Student> studentSet3 = new HashSet<>();
//        studentSet3.add(student2);
//        studentSet3.add(student3);
//        Set<Student> studentSet4 = new HashSet<>();
//        studentSet4.add(student1);
//        studentSet4.add(student2);
//        studentSet4.add(student3);
//
//        // setting students to courses
//        course1.setStudents(studentSet1);
//        course2.setStudents(studentSet4);
//        course3.setStudents(studentSet3);
//
//        // creating and saving details
//        Details details1 = new Details();
//        details1.setPrice(800);
//        details1.setDuration(500);
//        Details details2 = new Details();
//        details2.setPrice(1500);
//        details2.setDuration(600);
//        Details details3 = new Details();
//        details3.setPrice(1700);
//        details3.setDuration(700);
//
//        session.save(details1);
//        session.save(details2);
//        session.save(details3);
//
//        // setting details to courses
//        course1.setDetails(details1);
//        course2.setDetails(details2);
//        course3.setDetails(details3);

        Student student = session.createQuery("SELECT s FROM Student s WHERE s.name='Vitalij'", Student.class)
                .getSingleResult();
        System.out.println(student);
        System.out.println(student.getName() + " has courses: ");
        Set<Course> courses = student.getCourses();

        courses.forEach(course -> System.out.println(course.getName()));

        // committing
        session.getTransaction().commit();

        // shutting down
        HibernateUtil.shutdown();
    }
}