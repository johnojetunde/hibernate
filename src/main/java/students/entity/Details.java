package students.entity;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Table(schema = "university", name = "course_details")
@Getter
@Setter
public class Details {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "price")
    private int price;

    @Column(name = "duration")
    private int duration;

    @OneToOne(mappedBy = "details")
    private Course course;

    // getters and setters replaced by Lombok annotations @Getter and @Setter
}